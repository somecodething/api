package me.Swedz.api.entity;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.Swedz.api.API;

public class Fireworks implements iFireworks {
	public void spawn(Location loc, Type type, boolean flicker, boolean trail, boolean instant, Color... colors) {
		Firework fw = (Firework) loc.getWorld().spawnEntity(loc, EntityType.FIREWORK);
		FireworkMeta fm = fw.getFireworkMeta();
		
		fm.addEffect(FireworkEffect.builder()
			.with(type)
			.flicker(flicker)
			.trail(trail)
			.withColor(colors)
		.build());
		fm.setPower(0);
		
		fw.setFireworkMeta(fm);
		
		if(instant == true) {
			new BukkitRunnable() {
			    @Override
			    public void run() {
			      fw.detonate();
			    }
			}.runTaskLater(API.instance, 2L);
		}
	}
}