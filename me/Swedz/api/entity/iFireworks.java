package me.Swedz.api.entity;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.FireworkEffect.Type;

public interface iFireworks {
	public void spawn(Location loc, Type type, boolean flicker, boolean trail, boolean instant, Color... colors);
}
