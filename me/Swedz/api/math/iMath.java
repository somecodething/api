package me.Swedz.api.math;

public interface iMath {
	public Long getHowMuchLonger(Long startingTime, Long waitTime);
	public String secondsToString(long millisconds);
}