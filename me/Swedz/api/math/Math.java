package me.Swedz.api.math;

public class Math {
	public Long getHowMuchLonger(Long startingTime, Long waitTime) {
		return waitTime-(System.currentTimeMillis()-startingTime);
	}
	
	public String secondsToString(long milliseconds) {
		long seconds = milliseconds / 1000;
		long minutes = seconds / 60;
		long hours = minutes / 60;
		long days = hours / 24;
		String time = days + "d " + (hours % 24) + "h " + (minutes % 60) + "m " + (seconds % 60) + "s";

		if(time.split("").length >= 3) {
			if(time.split("")[0].equals("0") && time.split("")[1].equals("d") && time.split("")[2].equals(" ")) {
				time = time.replace("0d ", "");
			}
			if(time.split("")[0].equals("0") && time.split("")[1].equals("h") && time.split("")[2].equals(" ")) {
				time = time.replace("0h ", "");
			}
			if(time.split("")[0].equals("0") && time.split("")[1].equals("m") && time.split("")[2].equals(" ")) {
				time = time.replace("0m ", "");
			}
			if(time.split("")[0].equals("0") && time.split("")[1].equals("s") && time.split("")[2].equals(" ")) {
				time = time.replace(" 0s", "");
				time = time.replace("0s", "");
			}
		} else {
			time = "0s";
		}
		
		if(time.equalsIgnoreCase("")) {
			time = "null";
		}
		
		return time;
	}
	
	public String secondsToString(int milliseconds) {
		return secondsToString(Long.parseLong(milliseconds+""));
	}
}