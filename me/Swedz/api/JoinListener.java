package me.Swedz.api;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;

public class JoinListener implements Listener {
	@EventHandler
	public void onJoin(PlayerJoinEvent e) throws Exception {
		Player player = e.getPlayer();
		
		String uuid = API.getUniqueIdentifier().getUUID(player.getName());
		
		PacketPlayOutPlayerListHeaderFooter headerfooter = new PacketPlayOutPlayerListHeaderFooter();
		java.lang.reflect.Field header = headerfooter.getClass().getDeclaredField("a");
	    java.lang.reflect.Field footer = headerfooter.getClass().getDeclaredField("b");
	    header.setAccessible(true);
	    footer.setAccessible(true);
	    header.set(headerfooter, ChatSerializer.a("\"�fWelcome to the �r�e�lMCCUBED NETWORK�r\""));
	    footer.set(headerfooter, ChatSerializer.a("\"�fWebsite: �r�bmc-cubed.org�r    �fTwitter:�r �b@TwittCubed�r\n�r�fDiscord: �r�bdiscord.gg/qSDtJag    �r�fIP: �r�bplay.mc-cubed.org�r\""));
	    CraftPlayer cplayer = (CraftPlayer) e.getPlayer();
		cplayer.getHandle().playerConnection.sendPacket(headerfooter);
		
		if(uuid == null) {
			API.getSQLConnection().executeQuery("post", "replace into uuids (uuid, name) values ('" + player.getUniqueId() + "','" + player.getName() + "');");
			uuid = API.getUniqueIdentifier().getUUID(player.getName());
		} else {
			API.getSQLConnection().executeQuery("post", "update uuids set name = '" + player.getName() + "' where uuid = '" + uuid + "';");
		}
		
		String ip = (player.getAddress()+"").replace("/", "");
		String[] ip_s = ip.split(":");
		ip = ip_s[0];
		String check2 = API.getSQLConnection().executeQuery("get", "select ip from ips where ip = '" + ip + "' and uuid = '" + uuid + "';");
		if(check2 == null) {
			API.getSQLConnection().executeQuery("post", "replace into ips (uuid, ip) values ('" + uuid + "','" + ip + "');");
		}
		API.getSQLConnection().executeQuery("post", "delete from ips where ip = 'null';");
		API.getSQLConnection().executeQuery("post", "delete from ips where uuid = 'null';");
		
		String settingsCheck = API.getSQLConnection().executeQuery("get", "select uuid from settings where uuid = '" + uuid + "';");
		if(settingsCheck == null) {
			API.getSQLConnection().executeQuery("post", "replace into settings (uuid, filter, bow, chat) values ('" + uuid + "','true','true','true');");
		}
	}
}
