package me.Swedz.api.gold;

import me.Swedz.api.API;

public class Gold implements iGold {
	public int getGold(String player) throws Exception {
		String uuid = API.getUniqueIdentifier().getUUID(player);
		String value = API.getSQLConnection().executeQuery("get", "select gold from gold where uuid = '" + uuid + "';");
		if(value == null) {
			return 0;
		} else {
			return Integer.parseInt(value);
		}
	}
	
	public boolean hasGold(String player, int amount) throws Exception {
		String uuid = API.getUniqueIdentifier().getUUID(player);
		String value = API.getSQLConnection().executeQuery("get", "select gold from gold where uuid = '" + uuid + "';");
		if(value == null) {
			return false;
		} else {
			if(amount > Integer.parseInt(value)) {
				return false;
			} else {
				return true;
			}
		}
	}
	
	public void giveGold(String player, int amount) throws Exception {
		String uuid = API.getUniqueIdentifier().getUUID(player);
		int currentGold = getGold(player);
		if(currentGold == 0) {
			API.getSQLConnection().executeQuery("post", "replace into gold (uuid, gold) values ('" + uuid + "','" + amount + "');");
		} else {
			API.getSQLConnection().executeQuery("post", "update gold set gold = '" + (currentGold+amount) + "' where uuid = '" + uuid + "';");
		}
	}
	
	public void removeGold(String player, int amount) throws Exception {
		String uuid = API.getUniqueIdentifier().getUUID(player);
		int currentGold = getGold(player);
		
		int newGold = currentGold-amount;
		if(newGold <= 0) {
			API.getSQLConnection().executeQuery("post", "delete from gold where uuid = '" + uuid + "';");
		} else {
			API.getSQLConnection().executeQuery("post", "update gold set gold = '" + newGold + "' where uuid = '" + uuid + "';");
		}
	}
}