package me.Swedz.api.gold;

public interface iGold {
	public int getGold(String player) throws Exception;
	public boolean hasGold(String player, int amount) throws Exception;
	public void giveGold(String player, int amount) throws Exception;
	public void removeGold(String player, int amount) throws Exception;
}