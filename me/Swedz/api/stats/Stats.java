package me.Swedz.api.stats;

import me.Swedz.api.API;

public class Stats implements iStats {
	public int getStats(String uuid, String type) throws Exception {
		String curr = API.getSQLConnection().executeQuery("get", "select " + type + " from stats where uuid = '" + uuid + "';");
		if(curr == null) {
			return 0;
		} else {
			return Integer.parseInt(curr);
		}
	}
	
	public void addStat(String uuid, String type) throws Exception {
		int curr = getStats(uuid, type);
		if(curr == 0) {
			String typeOther = "";
			if(type == "kills") {
				typeOther = "deaths";
			} else if(type == "deaths") {
				typeOther = "kills";
			}
			API.getSQLConnection().executeQuery("post", "replace into stats (uuid, " + type + ", " + typeOther + ") values ('" + uuid + "','1','0');");
		} else {
			API.getSQLConnection().executeQuery("post", "update stats set " + type + " = '" + (curr+1) + "' where uuid = '" + uuid + "';");
		}
	}
}