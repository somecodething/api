package me.Swedz.api.stats;

public interface iStats {
	public int getStats(String uuid, String type) throws Exception;
	public void addStat(String uuid, String type) throws Exception;
}