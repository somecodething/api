package me.Swedz.api.items;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagInt;
import net.minecraft.server.v1_8_R3.NBTTagList;
import net.minecraft.server.v1_8_R3.NBTTagString;

public class Items implements iItems {
	public ItemStack createItem(ItemStack itemStack, String displayName, String[] lore, HashMap<Enchantment, Integer> enchants, boolean unbreakable, boolean hideStuff) {
		ItemMeta im = itemStack.getItemMeta();
		if(displayName != null) {
			im.setDisplayName(displayName);
		} if(lore != null) {
			im.setLore(Arrays.asList(lore));
		} if(enchants != null) {
			for(Enchantment le : enchants.keySet()) {
				im.addEnchant(le, enchants.get(le), true);
			}
		} if(hideStuff == true) {
			im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			im.addItemFlags(ItemFlag.HIDE_DESTROYS);
			im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			im.addItemFlags(ItemFlag.HIDE_PLACED_ON);
			im.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
			im.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
		}
		im.spigot().setUnbreakable(unbreakable);
		itemStack.setItemMeta(im);
		
		return itemStack;
	}
	
	public ItemStack applyAttribute(ItemStack item, String att, int amount) {
		net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
		NBTTagCompound cmp = nmsStack.getTag();
		if(cmp == null) {
			cmp = new NBTTagCompound();
			nmsStack.setTag(cmp);
			cmp = nmsStack.getTag();
		}
		NBTTagList modifiers = new NBTTagList();
		NBTTagCompound armor = new NBTTagCompound();
		armor.set("AttributeName", new NBTTagString(att));
		armor.set("Name", new NBTTagString(att));
		armor.set("Amount", new NBTTagInt(amount));
		armor.set("Operation", new NBTTagInt(0));
		armor.set("UUIDLeast", new NBTTagInt(1));
		armor.set("UUIDMost", new NBTTagInt(new Random().nextInt((999999999 - 1) + 1) + 1));
		modifiers.add(armor);
		cmp.set("AttributeModifiers", modifiers);
		nmsStack.setTag(cmp);
		item = CraftItemStack.asBukkitCopy(nmsStack);
		return item;
	}
	
	public ItemStack setSkullTexture(ItemStack itemStack, String url) throws Exception {
		SkullMeta hiSM = (SkullMeta) itemStack.getItemMeta();
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
		profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
		Field profileField = null;
		profileField = hiSM.getClass().getDeclaredField("profile");
		profileField.setAccessible(true);
		profileField.set(hiSM, profile);
		itemStack.setItemMeta(hiSM);
		return itemStack;
	}
}