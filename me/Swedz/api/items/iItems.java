package me.Swedz.api.items;

import java.util.HashMap;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public interface iItems {
	public ItemStack createItem(ItemStack itemStack, String displayName, String[] lore, HashMap<Enchantment, Integer> enchants, boolean unbreakable, boolean hideStuff);
	public ItemStack applyAttribute(ItemStack item, String att, int amount);
	public ItemStack setSkullTexture(ItemStack itemStack, String url) throws Exception;
}