package me.Swedz.api.levels;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.Player;

import me.Swedz.api.API;

public class Levels implements iLevels {
	public static ArrayList<Player> levelingUp = new ArrayList<Player>();
	
	public void giveXP(Player player, int add) throws Exception {
		String uuid = API.getUniqueIdentifier().getUUID(player.getName());
		int xp = getXP(player);
		xp = xp+add;
		int level = 0;
		if(inDB(player) == true) {
			level = getLevel(player);
			API.getSQLConnection().executeQuery("post", "update levels set xp = '" + xp + "' where uuid = '" + uuid + "';");
		} else {
			API.getSQLConnection().executeQuery("post", "replace into levels (uuid,level,xp) values ('" + uuid + "','0','" + xp + "');");
		}
		
		if(xp >= ((level*(level*10))+1000)) {
    		levelUp(player,xp);
    	}
	}
	
	public boolean inDB(Player player) throws Exception {
		String check = API.getSQLConnection().executeQuery("get", "select level from levels where uuid = '" + API.getUniqueIdentifier().getUUID(player.getName()) + "';");
		if(check == null) {
			return false;
		} else {
			return true;
		}
	}
	
	public void levelUp(Player player, int newXP) throws Exception {
		if(levelingUp.contains(player)) {
			return;
		} levelingUp.add(player);
		
		String uuid = API.getUniqueIdentifier().getUUID(player.getName());
		String level = API.getSQLConnection().executeQuery("get", "select level from levels where uuid = '" + uuid + "';");
		if(level == null) {
			API.getSQLConnection().executeQuery("post", "replace into levels (uuid,level,xp) values ('" + uuid + "','1','0');");
			level = "0";
		} else {
			if(Integer.parseInt(level) >= 100) {
				levelingUp.remove(player);
				return;
			}
			
			int intLevel = Integer.parseInt(level);
			API.getSQLConnection().executeQuery("post", "update levels set level = '" + (intLevel+1) + "' where uuid = '" + uuid + "';");
			API.instance.getServer().getScheduler().scheduleSyncDelayedTask(API.instance, new Runnable() {
				public void run() {
					int setXP = newXP-((intLevel*(intLevel*10))+1000);
					
					try {
						API.getSQLConnection().executeQuery("post", "update levels set xp = '" + setXP + "' where uuid = '" + uuid + "';");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}, 5);
		}
		
		Bukkit.broadcastMessage(API.getChatAPI().namecolor(player.getName()) + player.getName() + " §aleveled up from level " + getColor(Integer.parseInt(level)) + level + " §ato " + getColor((Integer.parseInt(level)+1)) + (Integer.parseInt(level)+1) + "§a!");
		
		levelingUp.remove(player);
		
		Color fireworkColor = Color.GRAY;
		Type fireworkType = Type.BURST;
		String levelColor = API.getLevelAPI().getColor(Integer.parseInt(level)+1);
		if(levelColor.equalsIgnoreCase("§9")) {
			fireworkColor = Color.BLUE;
		} else if(levelColor.equalsIgnoreCase("§6")) {
			fireworkColor = Color.ORANGE;
			fireworkType = Type.BALL;
		} else if(levelColor.equalsIgnoreCase("§c")) {
			fireworkColor = Color.RED;
			fireworkType = Type.BALL_LARGE;
		} else if(levelColor.equalsIgnoreCase("§4")) {
			fireworkColor = Color.MAROON;
			fireworkType = Type.STAR;
		} API.getFireworkAPI().spawn(player.getLocation(), fireworkType, true, false, true, fireworkColor);
	} 
	
	public int getLevel(Player player) throws Exception {
		int level = 0;
		String uuid = API.getUniqueIdentifier().getUUID(player.getName());
		String levelS = API.getSQLConnection().executeQuery("get", "select level from levels where uuid = '" + uuid + "';");
		if(levelS != null) {
			level = Integer.parseInt(levelS);
		}
		
		return level;
	}
	
	public int getXP(Player player) throws Exception {
		int xp = 0;
		String uuid = API.getUniqueIdentifier().getUUID(player.getName());
		String xpS = API.getSQLConnection().executeQuery("get", "select xp from levels where uuid = '" + uuid + "';");
		int level = getLevel(player);
		if(level >= 100) {
			return 0;
		}
		
		if(xpS != null) {
			xp = Integer.parseInt(xpS);
		}
		
		return xp;
	}
	
	public String getColor(int level) {
		if(level >= 0 && level <= 19) {
			return "§7";
		} else if(level >= 20 && level <= 39) {
			return "§9";
		} else if(level >= 40 && level <= 59) {
			return "§6";
		} else if(level >= 60 && level <= 79) {
			return "§c";
		} else if(level >= 80 && level <= 100) {
			return "§4";
		} else {
			return "§7";
		}
	}
	
	public boolean checkLevelup(Player player) throws Exception {
		int xp = getXP(player);
		int level = getLevel(player);
		
		if(level < 100) {
    		if(xp >= ((level*(level*10))+1000)) {
    			return true;
    		}
		}
		
		return false;
	}
}