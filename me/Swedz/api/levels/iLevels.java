package me.Swedz.api.levels;

import org.bukkit.entity.Player;

public interface iLevels {
	public void levelUp(Player player, int newXP) throws Exception;
	public int getLevel(Player player) throws Exception;
	public int getXP(Player player) throws Exception;
	public String getColor(int level);
	public boolean checkLevelup(Player player) throws Exception;
	public void giveXP(Player player, int add) throws Exception;
	public boolean inDB(Player player) throws Exception;
}