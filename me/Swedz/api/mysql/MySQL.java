package me.Swedz.api.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.scheduler.BukkitRunnable;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import me.Swedz.api.API;

public class MySQL implements iMySQL {
	Connection connection = null;
	
	public void createPool() throws Exception {
		MysqlDataSource ds = new MysqlDataSource();
		ds.setUser("server");
		ds.setDatabaseName("serverData");
		ds.setPassword("*******");
		ds.setServerName("localhost");
		ds.setPort(3306);
		ds.setConnectTimeout(10*1000);
		connection = ds.getConnection();
	}
	
	public void closePool() throws Exception {
		if(connection != null) {
			if(!connection.isClosed()) {
				connection.close();
			}
		}
	}
	
	public Connection getPool() {
		return connection;
	}
	
	public String executeQuery(String type, String query) throws Exception {
		String value = null;
		
		if(connection != null) {
			if(type.equalsIgnoreCase("get")) {
				PreparedStatement pst = connection.prepareStatement(query);
				ResultSet rs = pst.executeQuery();
				String[] column_split = query.split(" ");
				String column = column_split[1];
				if(rs.next()) {
					value = rs.getString(column);
				}
			} else {
				new BukkitRunnable() {
					@Override
					public void run() {
						PreparedStatement pst;
						try {
							pst = connection.prepareStatement(query);
							pst.executeUpdate();
						} catch (SQLException e) {
							e.printStackTrace();
						}
					}
				}.runTaskAsynchronously(API.instance);
			}
		}
		
		return value;
	}
	
	public ResultSet getRS(String query) throws SQLException {
		ResultSet value = null;
		if(connection != null) {
			PreparedStatement pst = connection.prepareStatement(query);
			value = pst.executeQuery();
		}
		return value;
	}
}
