package me.Swedz.api.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface iMySQL {
	public void createPool() throws Exception;
	public void closePool() throws Exception;
	public String executeQuery(String type, String query) throws Exception;
	public ResultSet getRS(String query) throws SQLException;
	public Connection getPool();
}
