package me.Swedz.api.rating;

import me.Swedz.api.API;

public class Ratings implements iRatings {
	public void addRating(String player, int amount) throws Exception {
		String check = API.getSQLConnection().executeQuery("get","select uuid from score where uuid = '" + API.getUniqueIdentifier().getUUID(player) + "';");
		if(check != null) {
			int amount_original = Integer.parseInt(API.getSQLConnection().executeQuery("get","select rating from score where uuid = '" + API.getUniqueIdentifier().getUUID(player) + "';"));
			int amount_new = amount+amount_original;
			if(amount_new > 10000) {
				amount_new = 10000;
			}
			
			API.getSQLConnection().executeQuery("post","update score set rating = '" + amount_new + "' where uuid = '" + API.getUniqueIdentifier().getUUID(player) + "';");
			return;
		} else {
			API.getSQLConnection().executeQuery("post","replace into score (uuid, rating) values ('" + API.getUniqueIdentifier().getUUID(player) + "','" + (1000+amount) + "');");
		}
	}
	
	public String getScore(String player) throws Exception {
		String score = "1";
		String rating = API.getSQLConnection().executeQuery("get", "select rating from score where uuid = '" + API.getUniqueIdentifier().getUUID(player) + "';");
		String[] rating_s = null;
		if(rating != null) {
			rating_s = API.getSQLConnection().executeQuery("get", "select rating from score where uuid = '" + API.getUniqueIdentifier().getUUID(player) + "';").split("");
		} else {
			rating_s = new String[] {"1","0","0","0"};
		}
		if(rating_s.length == 5) {
			score = "10";
		} else if(rating_s.length == 4) {
			score = rating_s[0];
		} else if(rating_s.length == 3) {
			score = "0";
		}
		return score;
	}
	
	public void subRating(String player, int amount) throws Exception {
		String check = API.getSQLConnection().executeQuery("get","select uuid from score where uuid = '" + API.getUniqueIdentifier().getUUID(player) + "';");
		if(check != null) {
			int amount_original = Integer.parseInt(API.getSQLConnection().executeQuery("get","select rating from score where uuid = '" + API.getUniqueIdentifier().getUUID(player) + "';"));
			int amount_new = amount_original-amount;
			if(amount_new < 0) {
				amount_new = 0;
			}
			
			API.getSQLConnection().executeQuery("post","update score set rating = '" + amount_new + "' where uuid = '" + API.getUniqueIdentifier().getUUID(player) + "';");
			return;
		} else {
			API.getSQLConnection().executeQuery("post","replace into score (uuid, rating) values ('" + API.getUniqueIdentifier().getUUID(player) + "','" + (1000-amount) + "');");
		}
		return;
	}
}
