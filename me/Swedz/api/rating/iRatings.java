package me.Swedz.api.rating;

public interface iRatings {
	public void addRating(String player, int amount) throws Exception;
	public String getScore(String player) throws Exception;
	public void subRating(String player, int amount) throws Exception;
}
