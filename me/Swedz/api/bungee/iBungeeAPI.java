package me.Swedz.api.bungee;

import org.bukkit.entity.Player;

public interface iBungeeAPI {
	public void connect(Player player, String server);
	public void sendBungeeMessage(String player, String message);
	public void kickBungeePlayer(String player, String reason);
	public String getBungeeCount(String server) throws Exception;
	public String[] getAllServers();
}
