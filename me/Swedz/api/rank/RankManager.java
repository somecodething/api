package me.Swedz.api.rank;

import me.Swedz.api.API;

public class RankManager {
	public String getRank(String player) throws Exception {
		String rank = API.getSQLConnection().executeQuery("get","select rank from ranks where uuid = '" + API.getUniqueIdentifier().getUUID(player) + "';");
		if(rank == null) {
			rank = "default";
		}
		
		return rank;
	}
	
	public boolean getPerm(String player, String perm) throws Exception {
		boolean value = false;
		
		String rank = getRank(player);
		if(perm.equalsIgnoreCase("highest")) {
			if(rank.equalsIgnoreCase("owner")) {
				value = true;
			}
		} else if(perm.equalsIgnoreCase("high")) {
			if(rank.equalsIgnoreCase("admin") || rank.equalsIgnoreCase("dev") || rank.equalsIgnoreCase("owner")) {
				value = true;
			}
		} else if(perm.equalsIgnoreCase("host")) {
			if(rank.equalsIgnoreCase("admin") || rank.equalsIgnoreCase("dev") || rank.equalsIgnoreCase("host") || rank.equalsIgnoreCase("owner")) {
				value = true;
			}
		} else if(perm.equalsIgnoreCase("staff")) {
			if(rank.equalsIgnoreCase("admin") || rank.equalsIgnoreCase("dev") || rank.equalsIgnoreCase("srmod") || rank.equalsIgnoreCase("mod") || rank.equalsIgnoreCase("helper") || rank.equalsIgnoreCase("host") || rank.equalsIgnoreCase("trial") || rank.equalsIgnoreCase("owner")) {
				value = true;
			}
		} else if(perm.equalsIgnoreCase("staff-high")) {
			if(rank.equalsIgnoreCase("admin") || rank.equalsIgnoreCase("dev") || rank.equalsIgnoreCase("srmod") || rank.equalsIgnoreCase("owner") || rank.equalsIgnoreCase("host")) {
				value = true;
			}
		} else if(perm.equalsIgnoreCase("staff-med")) {
			if(rank.equalsIgnoreCase("admin") || rank.equalsIgnoreCase("dev") || rank.equalsIgnoreCase("srmod") || rank.equalsIgnoreCase("mod") || rank.equalsIgnoreCase("host") || rank.equalsIgnoreCase("owner")) {
				value = true;
			}
		} else if(perm.equalsIgnoreCase("staff-low")) {
			if(rank.equalsIgnoreCase("admin") || rank.equalsIgnoreCase("dev") || rank.equalsIgnoreCase("srmod") || rank.equalsIgnoreCase("mod") || rank.equalsIgnoreCase("helper") || rank.equalsIgnoreCase("host") || rank.equalsIgnoreCase("owner")) {
				value = true;
			}
		} else if(perm.equalsIgnoreCase("pro")) {
			if(!rank.equalsIgnoreCase("default") || !rank.equalsIgnoreCase("donator") || !rank.equalsIgnoreCase("donator-plus")) {
				value = true;
			}
		} else if(perm.equalsIgnoreCase("donator")) {
			if(!rank.equalsIgnoreCase("default")) {
				value = true;
			}
		}
		
		return value;
	}
}
