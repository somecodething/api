package me.Swedz.api.rank;

public interface iRankManager {
	public String getRank(String player) throws Exception;
	public boolean getPerm(String player, String perm) throws Exception;
}
