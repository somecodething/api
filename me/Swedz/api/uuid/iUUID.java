package me.Swedz.api.uuid;

public interface iUUID {
	public String getUUID(String player) throws Exception;
	public String getName(String uuid) throws Exception;
}
