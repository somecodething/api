package me.Swedz.api.utils;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;

public class Utils implements iUtils {
	public void sendTitle(Player player, String top, String bottom, int in, int stay, int out) {
		PacketPlayOutTitle packet1 = new PacketPlayOutTitle(in, stay, out);
		PacketPlayOutTitle packet2 = new PacketPlayOutTitle(EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a(top));
		PacketPlayOutTitle packet3 = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a(bottom));
		
		CraftPlayer cp = (CraftPlayer) player;
		cp.getHandle().playerConnection.sendPacket(packet1);
		cp.getHandle().playerConnection.sendPacket(packet2);
		cp.getHandle().playerConnection.sendPacket(packet3);
	}
	
	public void sendActionBar(Player player, String message) {
	    IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \"" + message + "\"}");
	    PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc,(byte) 2);
	    ((CraftPlayer) player).getHandle().playerConnection.sendPacket(ppoc);
	}
}