package me.Swedz.api.chat;

public interface iChat {
	public String namecolor(String player) throws Exception;
	public String chatcolor(String player) throws Exception;
	public String colorize(String str);
	public String prefix(String type, String player) throws Exception;
	public String stripColor(String str);
}
