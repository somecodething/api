package me.Swedz.api.chat;

import me.Swedz.api.API;

public class Chat implements iChat {
	public String namecolor(String player) throws Exception {
		String value = null;
		String nc = API.getSQLConnection().executeQuery("get","select color from namecolor where uuid = '" + API.getUniqueIdentifier().getUUID(player) + "';");
		if(nc != null) {
			value = nc.replace("&", "�");
		} else {
			value = "�7";
		}
		return value;
	}
	
	public String chatcolor(String player) throws Exception {
		String value = "�f";
		if(API.getRankManagement().getPerm(player,"staff") == true) {
			value = "�e";
		}
		return value;
	}
	
	public String colorize(String str) {
		String new_str = str;

		new_str = new_str.replaceAll("&a", "�a");
		new_str = new_str.replaceAll("&b", "�b");
		new_str = new_str.replaceAll("&c", "�c");
		new_str = new_str.replaceAll("&d", "�d");
		new_str = new_str.replaceAll("&e", "�e");
		new_str = new_str.replaceAll("&f", "�f");
		new_str = new_str.replaceAll("&m", "�m");
		new_str = new_str.replaceAll("&l", "�l");
		new_str = new_str.replaceAll("&n", "�n");
		new_str = new_str.replaceAll("&r", "�r");
		new_str = new_str.replaceAll("&k", "�k");
		new_str = new_str.replaceAll("&o", "�o");
		new_str = new_str.replaceAll("&1", "�1");
		new_str = new_str.replaceAll("&2", "�2");
		new_str = new_str.replaceAll("&3", "�3");
		new_str = new_str.replaceAll("&4", "�4");
		new_str = new_str.replaceAll("&5", "�5");
		new_str = new_str.replaceAll("&6", "�6");
		new_str = new_str.replaceAll("&7", "�7");
		new_str = new_str.replaceAll("&8", "�8");
		new_str = new_str.replaceAll("&9", "�9");
		
		return new_str;
		//return str.replaceAll("(?i)&([a-f0-9])", "�$1");
	}
	
	public String stripColor(String str) {
		String new_str = str;

		new_str = new_str.replaceAll("�a", "");
		new_str = new_str.replaceAll("�b", "");
		new_str = new_str.replaceAll("�c", "");
		new_str = new_str.replaceAll("�d", "");
		new_str = new_str.replaceAll("�e", "");
		new_str = new_str.replaceAll("�f", "");
		new_str = new_str.replaceAll("�m", "");
		new_str = new_str.replaceAll("�l", "");
		new_str = new_str.replaceAll("�n", "");
		new_str = new_str.replaceAll("�r", "");
		new_str = new_str.replaceAll("�k", "");
		new_str = new_str.replaceAll("�o", "");
		new_str = new_str.replaceAll("�1", "");
		new_str = new_str.replaceAll("�2", "");
		new_str = new_str.replaceAll("�3", "");
		new_str = new_str.replaceAll("�4", "");
		new_str = new_str.replaceAll("�5", "");
		new_str = new_str.replaceAll("�6", "");
		new_str = new_str.replaceAll("�7", "");
		new_str = new_str.replaceAll("�8", "");
		new_str = new_str.replaceAll("�9", "");
		
		return new_str;
	}
	
	public String prefix(String type, String player) throws Exception {
		String value = null;
		if(type.equalsIgnoreCase("main")) {
			String rank = API.getRankManagement().getRank(player);
			if(rank.equalsIgnoreCase("owner")) {
				value = "�4[Owner]";
			} else if(rank.equalsIgnoreCase("admin")) {
				value = "�4[Admin]";
			} else if(rank.equalsIgnoreCase("dev")) {
				value = "�5[Dev]";
			} else if(rank.equalsIgnoreCase("srmod")) {
				value = "�c[Sr.Mod]";
			} else if(rank.equalsIgnoreCase("mod")) {
				value = "�6[Mod]";
			} else if(rank.equalsIgnoreCase("host")) {
				value = "�c[Host]";
			} else if(rank.equalsIgnoreCase("helper")) {
				value = "�3[Helper]";
			} else if(rank.equalsIgnoreCase("trial")) {
				value = "�3[Trial]";
			} else if(rank.equalsIgnoreCase("famous") || rank.equalsIgnoreCase("famous+")) {
				value = "�5[Famous]";
			} else if(rank.equalsIgnoreCase("donator-pro")) {
				value = "�a[$$$]";
			} else if(rank.equalsIgnoreCase("donator-plus")) {
				value = "�a[$$]";
			} else if(rank.equalsIgnoreCase("donator")) {
				value = "�a[$]";
			}
		} else if(type.equalsIgnoreCase("bonus")) {
			String value0 = API.getSQLConnection().executeQuery("get","select pre from prefix2 where uuid = '" + API.getUniqueIdentifier().getUUID(player) + "';");
			if(value0 == null) {
				value = "";
			} else {
				String[] value1 = value0.split("");
				value = "�" + value1[1] + "[" + value0.replace("&" + value1[1], "") + "]";
			}
		}
		
		if(value == null) {
			value = "";
		}
		return value;
	}
}
