package me.Swedz.api;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.Swedz.api.bungee.BungeeAPI;
import me.Swedz.api.chat.Chat;
import me.Swedz.api.entity.Fireworks;
import me.Swedz.api.gold.Gold;
import me.Swedz.api.items.Items;
import me.Swedz.api.levels.Levels;
import me.Swedz.api.mysql.MySQL;
import me.Swedz.api.rank.RankManager;
import me.Swedz.api.rating.Ratings;
import me.Swedz.api.stats.Stats;
import me.Swedz.api.utils.Utils;
import me.Swedz.api.uuid.UUID;

public class API extends JavaPlugin {
	private static MySQL sqlConnection = null;
	public static MySQL getSQLConnection() { return sqlConnection; }

	private static Chat chatAPI = null;
	public static Chat getChatAPI() { return chatAPI; }

	private static UUID uuidAPI = null;
	public static UUID getUniqueIdentifier() { return uuidAPI; }

	private static RankManager rankManagerAPI = null;
	public static RankManager getRankManagement() { return rankManagerAPI; }

	private static Items itemAPI = null;
	public static Items getItemAPI() { return itemAPI; }

	private static Ratings ratingAPI = null;
	public static Ratings getRatingAPI() { return ratingAPI; }

	private static BungeeAPI bungeeAPI = null;
	public static BungeeAPI getBungeeAPI() { return bungeeAPI; }

	private static Levels levelAPI = null;
	public static Levels getLevelAPI() { return levelAPI; }

	private static Fireworks fireworkAPI = null;
	public static Fireworks getFireworkAPI() { return fireworkAPI; }

	private static Gold goldAPI = null;
	public static Gold getGoldAPI() { return goldAPI; }

	private static me.Swedz.api.math.Math mathAPI = null;
	public static me.Swedz.api.math.Math getMathAPI() { return mathAPI; }

	private static Stats statsAPI = null;
	public static Stats getStatsAPI() { return statsAPI; }

	private static Utils utilsAPI = null;
	public static Utils getUtilsAPI() { return utilsAPI; }
	
	public static API instance;
	public void onEnable() {
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		
		instance = this;
		sqlConnection = new MySQL();
		chatAPI = new Chat();
		uuidAPI = new UUID();
		rankManagerAPI = new RankManager();
		itemAPI = new Items();
		ratingAPI = new Ratings();
		bungeeAPI = new BungeeAPI();
		levelAPI = new Levels();
		fireworkAPI = new Fireworks();
		goldAPI = new Gold();
		mathAPI = new me.Swedz.api.math.Math();
		statsAPI = new Stats();
		utilsAPI = new Utils();
		
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new BungeeAPI());
		
		try {
			getSQLConnection().createPool();
			getBungeeAPI().bungeeUpdater();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		registerEvents(instance,
			new JoinListener()
		);
	}
	
	public void onDisable() {
		Bukkit.getScheduler().cancelAllTasks();
		
		try {
			getSQLConnection().closePool();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
        for (Listener listener : listeners) {
            Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
        }
    }
}